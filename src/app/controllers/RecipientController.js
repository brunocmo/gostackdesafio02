import * as Yup from 'yup';
import Recipient from '../models/Recipient';

class RecipientController {
  async show(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }
    const nameExists = await Recipient.findOne({
      where: { name: req.body.name },
    });

    if (!nameExists) {
      return res.status(400).json({ error: 'Name does not exists' });
    }

    const {
      name,
      address,
      number,
      complement,
      state,
      city,
      cep,
    } = await Recipient.findOne({
      where: { name: req.body.name },
    });

    return res.json({
      name,
      address,
      number,
      complement,
      state,
      city,
      cep,
    });
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      address: Yup.string().required(),
      number: Yup.number().required(),
      complement: Yup.string().required(),
      state: Yup.string().required(),
      city: Yup.string().required(),
      cep: Yup.number()
        .min(8)
        .required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const nameExists = await Recipient.findOne({
      where: { name: req.body.name },
    });

    if (nameExists) {
      return res.status(400).json({ error: 'Name already exists' });
    }

    const {
      id,
      name,
      address,
      number,
      complement,
      state,
      city,
      cep,
    } = await Recipient.create(req.body);

    return res.json({
      id,
      name,
      address,
      number,
      complement,
      state,
      city,
      cep,
    });
  }

  async update(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validation fails' });
    }

    const nameExists = await Recipient.findOne({
      where: { name: req.body.name },
    });

    if (!nameExists) {
      return res.status(400).json({ error: 'Name does not exists' });
    }

    const recipient = await Recipient.findOne({
      where: { name: req.body.name },
    });

    const {
      id,
      name,
      address,
      number,
      complement,
      state,
      city,
      cep,
    } = await recipient.update(req.body);

    return res.json({
      id,
      name,
      address,
      number,
      complement,
      state,
      city,
      cep,
    });
  }
}

export default new RecipientController();
